using MarsRover.Core.Abstract;
using MarsRover.Core.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using static MarsRover.Core.Concrete.DirectionBase;

namespace MarsRover.UnitTest
{
    [TestClass]
    public class MarsRoverUnitTest
    {
        [TestMethod]
        public void TestScenarioOne()
        {
            PositionBase position = ServiceProvider.PositionInstance;
            
            position.Clear();
            position.X = 1;
            position.Y = 2;

            var points = new List<int> { 5, 5 };
            var moves = "LMLMLMLMM";

            position.Start(points, moves);

            Assert.AreEqual("1 3 N", $"{position}");
        }

        [TestMethod]
        public void TestScenarioTwo()
        {
            PositionBase position = ServiceProvider.PositionInstance;

            position.Clear();
            position.X = 3;
            position.Y = 3;
            position.Direction = Directions.E;

            var points = new List<int> { 5, 5 };
            var moves = "MMRMMRMRRM";

            position.Start(points, moves);

            Assert.AreEqual("5 1 E", $"{position}");
        }
    }
}
