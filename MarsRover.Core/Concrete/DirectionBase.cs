﻿
namespace MarsRover.Core.Concrete
{
    public class DirectionBase
    {
        public enum Directions
        {
            /// <summary>
            /// Kuzey-North
            /// </summary>
            N = 1,
            /// <summary>
            /// Güney-South
            /// </summary>
            S = 2,
            /// <summary>
            /// Doğu-East
            /// </summary>
            E = 3,
            /// <summary>
            /// Bat-West
            /// </summary>
            W = 4
        }
    }
}
