﻿using MarsRover.Core.Abstract;
using System;
using System.Collections.Generic;

namespace MarsRover.Core.Concrete
{
    public sealed class Position : PositionBase
    {
        public override void Start(List<int> points, string moves)
        {
            foreach (var move in moves)
            {
                switch (move.ToString().ToUpper())
                {
                    case "M":
                        this.Move();
                        break;
                    case "L":
                        this.RotateLeft();
                        break;
                    case "R":
                        this.RotateRight();
                        break;
                    default:
                        throw new ArgumentException($"Undefined Direction {move}");
                }

                if (this.X < 0 || this.X > points[0] || this.Y < 0 || this.Y > points[1])
                {
                    throw new ArgumentOutOfRangeException($"You have exceeded the point limits. ({points[0]} , {points[1]}) - ({this.X}, {this.Y})");
                }
            }
        }

        public override string ToString()
        {
            return $"{X} {Y} {Direction}";
        }
    }
}
