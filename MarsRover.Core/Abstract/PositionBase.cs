﻿using System.Collections.Generic;
using static MarsRover.Core.Concrete.DirectionBase;

namespace MarsRover.Core.Abstract
{
    public abstract class PositionBase
    {
        public int X { get; set; }
        public int Y { get; set; }
        public Directions Direction { get; set; } = Directions.N;

        protected void RotateLeft()
        {
            switch (this.Direction)
            {
                case Directions.N:
                    this.Direction = Directions.W;
                    break;
                case Directions.W:
                    this.Direction = Directions.S;
                    break;
                case Directions.S:
                    this.Direction = Directions.E;
                    break;
                case Directions.E:
                    this.Direction = Directions.N;
                    break;
                default:
                    break;
            }
        }

        protected void RotateRight()
        {
            switch (this.Direction)
            {
                case Directions.N:
                    this.Direction = Directions.E;
                    break;
                case Directions.E:
                    this.Direction = Directions.S;
                    break;
                case Directions.S:
                    this.Direction = Directions.W;
                    break;
                case Directions.W:
                    this.Direction = Directions.N;
                    break;
                default:
                    break;
            }
        }

        protected void Move()
        {
            switch (this.Direction)
            {
                case Directions.N:
                    this.Y += 1;
                    break;
                case Directions.S:
                    this.Y -= 1;
                    break;
                case Directions.E:
                    this.X += 1;
                    break;
                case Directions.W:
                    this.X -= 1;
                    break;
                default:
                    break;
            }
        }

        public abstract void Start(List<int> points, string moves);

        public void Clear()
        {
            this.X = 0;
            this.Y = 0;
            this.Direction= Directions.N;
        }
    }
}
