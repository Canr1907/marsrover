﻿using MarsRover.Core.Abstract;
using MarsRover.Core.Concrete;

namespace MarsRover.Core.Utilities
{
    public sealed class ServiceProvider
    {
        private readonly static object _lockObject = new object();
        private static PositionBase _positionBase = null;

        public static PositionBase PositionInstance { 
            get
            {
                lock (_lockObject)
                {
                    if (_positionBase == null)
                        _positionBase = new Position();
                }

                return _positionBase;
            } 
        }
    }
}
