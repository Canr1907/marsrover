﻿using MarsRover.Core.Abstract;
using MarsRover.Core.Utilities;
using System;
using System.Collections.Generic;
using static MarsRover.Core.Concrete.DirectionBase;

namespace MarsRover
{
    internal class Program
    {
        static void Main(string[] args)
        {
            try
            {
                PositionBase position = ServiceProvider.PositionInstance;
                position.X = 1;
                position.Y = 2;

                var point = new List<int> { 5, 5 };
                var moves = "LMLMLMLMM";

                Console.WriteLine($"Input 1: {position.ToString()}");

                position.Start(point, moves);

                Console.WriteLine($"Output 1: {position.ToString()}");

                position.Clear();

                Console.WriteLine("---------------------------------------");

                position.X = 3;
                position.Y = 3;
                position.Direction = Directions.E;

                moves = "MMRMMRMRRM";

                Console.WriteLine($"Input 2: {position.ToString()}");

                position.Start(point, moves);

                Console.WriteLine($"Output 2: {position.ToString()}");

                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine("\n");
                Console.WriteLine(ex.Message);
            }
        }
    }
}
